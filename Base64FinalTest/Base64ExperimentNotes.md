# How to use the tool?
**Use the ./base64[encode or decode] tool and then the file.
For example ./base64encode kittens.png**

# Why decoding is slower than encoding?
**If the computer is enconding, the computer and python know where to find the bits; however, if the computer is decoding it has to search for the characters and bits, and thats why it takes longer**

# Faster, Better
**The usage of a dictionary helps the computer to search through the file making it faster and better**


