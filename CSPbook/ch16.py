# Exercise 1 from Chapter 16 (using modification)
solid = input('Your name is?: ')
print('Exercise 1: ')
name = solid
start = "My name is "
combined = str(start) + str(name)
print(len(combined))
print(combined)
print(str(name) * 3)

# Exercise 2 from Chapter 16
print('Exercise 2: ')
a_string = "alto"
b_string = "bow"
c_string = (a_string + b_string) * 2
print(len(c_string))

# Exercise 3 from Chapter 16
print('Exercise 3: ')
my_first_list = [12,"ape",13]
print(len(my_first_list))
print(my_first_list * 3)
my_second_list = my_first_list + [321.4]
print(my_second_list)
print(len(my_second_list))

# Exercise 4 from Chapter 16 (modified)
print('Exercise 4: ')
def name_procedure(name):
    start = "Oh yes! I remember... Your name is "
    combined = str(start) + (str(name) * 2)
    print(combined)
    print(len(combined))
name_procedure(solid)

# Exercise 5 from Chapter 16
print('Exercise 5: ')
def item_lister(items):
    items[0] = "First item"
    items[1] = items[0]
    items[2] = items[2] + 1
    print(items)

item_lister([2, 4, 6, 8])

# Exercise 6 from Chapter 16
print('Exercise 6: ')
def grade_average(a_list):
    sum = 0
    for num in a_list:
        sum = sum + num
        average = sum/len(a_list)
        return average

a_list = [99, 100, 74, 63, 100, 100]
print(grade_average(a_list))

# Excercise 7 from Chapter 16
print('Exercise 7: ')
source = ["This", "is", "a", "list"]
so_far = []
for index in range(0,len(source)):
    so_far = [source[index]] + so_far
    print(so_far)

# Exercise 8 from chapter 16
print('Exercise 8: ')
items = ['hi', 2, 3, 4]
items[0] = items[0] * 2
items[1] = items[2] - 3
items[2] = items[1]
print(items)

# Exercise 9 from chapter 16
print('Exercise 9: ')
source = ["This", "is", "a", "list"]
so_far = []
for index in range(0,len(source)):
    so_far = [source[index]] + so_far
    print(so_far)

# Exercise 10 from chapter 16
print('Exercise 10: ')
source = ["This","is","a","list"]
so_far = [source]
for index in range(0,len(source)):
    # Add a list with the current item from source to so_far
    so_far =  [source[index]] + so_far
    print(so_far)

# Exercise 11 from chapter 16
print('Exercise 11: ')
numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
even_list = []
for index in range(2,len(numbers),2):
    even_list = even_list + [numbers[index]]
print(even_list)

# Exercise 12 from chapter 16
print('Exercise 12: ')
numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
evens = []
for index in range(5,len(numbers),5):
    evens = evens + [numbers[index]]
print(evens)

# Exercise 13 from chapter 16
print('Exercise 13: ')
for index in range(5, -1, -1):
    print(index)

# Exercise 14 from chapter 16
print('Exercise 14: ')
source = ["This", "is", "a", "list"]
new_list = []
for index in range(0, len(source)):
    new_list = new_list + [source[index]]
print(new_list)

# Exercise 15 from chapter 16
print('Exercise 15: ')
numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
odd_list = []
for index in range(1,len(numbers)):
    if index % 2 == 1:
        odd_list = odd_list + [numbers[index]]
print(odd_list)

# Exercise 16 from chapter 16
print('Exercise 16: ')
numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
go_list = []
for index in range(1,len(numbers)):
    go_list = go_list + ([numbers[index] + 5])
print(go_list)

# Exercise 17 from chapter 16
print('Exercise 17: ')
numbers = [0, 1, -2, 3, 4, 5, -6, 7, -8, -9, 10]
ready = 0
for index in range(1,len(numbers)):
     if numbers[index] >= 0:
        ready = ready + index
print(ready)

# Exercise 18 from chapter 16
print('Exercise 18: ')
numbers = [0, 1, -2, 3, 4, 5, -6, 7, -8, -9, 10]
pos = 0
neg = 0
for index in range(1,len(numbers)):
    if numbers[index] >= 0:
        pos = pos + index
    else:
        neg = neg + index
    ave = (pos + neg)/2
print('The sum of positive numbers on the list is: ' + str(pos))
print('The sum of negative numbers on the list is: ' + str(neg))
print("Average: " + str(ave))

# Exercise 19 from chapter 16
print('Exercise 19: ')

# Exercise 20 from chapter 16
print('Exercise 20: ')
