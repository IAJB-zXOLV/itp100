def only_evens(nums):
    """
      >>> only_evens([3, 8, 5, 4, 12, 7, 2])
      [8, 4, 12, 2]
      >>> my_nums = [4, 7, 19, 22, 42]
      >>> only_evens(my_nums)
      [4, 22, 42]
      >>> my_nums
      [4, 7, 19, 22, 42]
    """
    evens = []
    for i in nums:
        if i % 2 == 0:
            evens.append(i)
    return evens

def num_even_digits(number):
    """
      >>> num_even_digits(123456)
      3
      >>> num_even_digits(2468)
      4
      >>> num_even_digits(1357)
      0
      >>> num_even_digits(2)
      1
      >>> num_even_digits(20)
      2
    """
    count = 0
    for nums in str(number):
        if int(nums) % 2 == 0:
            count += 1
    return count

def sum_of_squares_of_digits(numberr):
    """
      >>> sum_of_squares_of_digits(1)
      1
      >>> sum_of_squares_of_digits(9)
      81
      >>> sum_of_squares_of_digits(11)
      2
      >>> sum_of_squares_of_digits(121)
      6
      >>> sum_of_squares_of_digits(987)
      194
    """
    total = 0
    for nums in str(numberr):
        sqr = int(nums) ** 2
        total += sqr
    return total

def lots_of_letters(wrd):
    """
      >>> lots_of_letters('Lidia')
      'Liidddiiiiaaaaa'
      >>> lots_of_letters('Python')
      'Pyyttthhhhooooonnnnnn'
      >>> lots_of_letters('')
      ''
      >>> lots_of_letters('1')
      '1'
    """
    outwrd = ""
    count = 1
    for i in str(wrd):
        outwrd += (i * count)
        count += 1
    return outwrd

def gcf(m, n):
    """
      >>> gcf(10, 25)
      5
      >>> gcf(8, 12)
      4
      >>> gcf(5, 12)
      1
      >>> gcf(24, 12)
      12
    """
    if n > m:
        count = n
    elif m > n:
        count = m
    while count > 0:
        if m % count == 0 and n % count == 0:
            return count
        else:
            count -= 1

def is_prime(x):
    """
      >>> is_prime(2)
      True
      >>> is_prime(7)
      True
      >>> is_prime(199)
      True
      >>> is_prime(12)
      False
      >>> is_prime(333)
      False
    """
    if x == 1 or x == 2 or x == 3 or x == 5 or x == 7:
        return True
    if x == 0:
        return "Is zero!"
    count = x - 1
    while count > 0:
        if x % 2 == 0:
            return False
        elif x % 3 == 0:
            return False
        else:
            return True
        count -= 1


if __name__ == '__main__':
    import doctest
    doctest.testmod()

