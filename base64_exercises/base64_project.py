def to_base(num, base):
    """
      >>> to_base(10, 3)
      '101'
      >>> to_base(11, 2)
      '1011'
      >>> to_base(10, 6)
      '14'
    """
    answer = ""
    while num:
        num, modulo = divmod(num, base)
        answer = str(modulo) + answer
    return answer

def to_binary(num):
    """
      >>> to_binary(10) 
      '1010'
      >>> to_binary(12) 
      '1100'
      >>> to_binary(0) 
      '0'
      >>> to_binary(1) 
      '1'
    """
    if num == 0:
        return '0'
    return to_base(num, 2) 

def to_base3(num):
    """
      >>> to_base3(10)
      '101'
      >>> to_base3(12)
      '110'
      >>> to_base3(6)
      '20'
    """
    return to_base(num, 3)

def to_base4(num):
    """
      >>> to_base4(20)
      '110'
      >>> to_base4(28)
      '130'
      >>> to_base4(3)
      '3'
    """
    return to_base(num, 4)



if __name__ == '__main__':
    import doctest
    doctest.testmod()

