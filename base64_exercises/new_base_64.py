def to_base(num, base):
    """
      >>> to_base(0, 12)
      '0'
      >>> to_base(10, 3)
      '101'
      >>> to_base(11, 2)
      '1011'
      >>> to_base(10, 6)
      '14'
      >>> to_base(21, 3)
      '210'
      >>> to_base(21, 11)
      '1A'
      >>> to_base(47, 16)
      '2F'
      >>> to_base(65535, 16)
      'FFFF'
      >>> to_base(23452345, 64)
      'BZdq5'
    """
    if num == 0:
        return '0'

    eo = '0123456789ABCDEF'

    if base == 64:
        eo = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
    numstr = ""
    while num:
        numstr = eo[num % base] + numstr
        num //= base
    return numstr 

if __name__ == '__main__':
    import doctest
    doctest.testmod()
