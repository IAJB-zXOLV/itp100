import io
def base64encode(f):

    s = ''
    data = f.read(3)

    while data:
        s += encode3bytes(data)
        data = f.read(3)

    return s
f1 = io.BytesIO(b'This is a test. It is only a test!')
base64encode(f1)
