def encode3bytes(bytes):
    """
      >>> encode3bytes(b'\\x5A\\x2B\\xE6')
      'Wivm'
      >>> encode3bytes(b'\\x49\\x33\\x8F')
      'STOP'
      >>> encode3bytes(b'\\xFF\\xFF\\xFF')
      '////'
      >>> encode3bytes(b'\\x00\\x00\\x00')
      'AAAA'
      >>> encode3bytes(b'T')
      'VA=='
      >>> encode3bytes(b'Te')
      'VGU='
      >>> encode3bytes(b'Tst')
      'VHN0'
      >>> encode3bytes(b'\\x00')
      'AA=='
      >>> encode3bytes(b'\\x00\\x00')
      'AAA='
      >>> encode3bytes(b'\\xFF')
      '/w=='
      >>> encode3bytes(b'\\xFF\\xFF')
      '//8='
    """
    digits = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'

    if len(bytes) < 1 or len(bytes) > 3:
        raise ValueError('Input should be 1 to 3 bytes')

    b1 = bytes[0]
    index1 = b1 >> 2

    if len(bytes) == 1:
        index2 = (b1 & 3) << 4
        return f'{digits[index1]}{digits[index2]}=='

    b2 = bytes[1]
    index2 = (b1 & 3) << 4 | b2 >> 4

    if len(bytes) == 2:
        index3 = (b2 & 15) << 2
        return f'{digits[index1]}{digits[index2]}{digits[index3]}='

    b3 = bytes[2]
    index3 = (b2 & 15) << 2 | (b3 & 192) >> 6
    index4 = b3 & 63
    return f'{digits[index1]}{digits[index2]}{digits[index3]}{digits[index4]}'


def decode4chars(s):
    """
      >>> decode4chars('STOP')
      b'I3\\x8f'
      >>> decode4chars('Wivm')
      b'Z+\xe6'
      >>> decode4chars('////')
      b'\xff\xff\xff'
      >>> decode4chars('VA==')
      b'T'
      >>> decode4chars('VGU=')
      b'Te'
      >>> decode4chars('AA==')
      b'\\x00'
      >>> decode4chars('AAA=')
      b'\\x00\\x00'
      >>> decode4chars('/w==')
      b'\\xff'
      >>> decode4chars('//8=')
      b'\\xff\\xff'
    """
    digits = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'

    if len(s) != 4 or not all([ch in digits + '=' for ch in s]):
        raise ValueError(f'{s} is not a value base64 encoded string')

    int1 = digits.index(s[0])
    int2 = digits.index(s[1])
    b1 = (int1 << 2) | ((int2 & 48) >> 4)

    if s[2:] == '==':
        return bytes([b1])

    int3 = digits.index(s[2])
    b2 = (int2 & 15) << 4 | int3 >> 2

    if s[3:] == '=':
        return bytes([b1, b2])

    int4 = digits.index(s[3])
    b3 = (int3 & 3) << 6 | int4

    return bytes([b1, b2, b3])


if __name__ == '__main__':
    import doctest
    doctest.testmod()



