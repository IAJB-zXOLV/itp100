import io 
from base64_tools.py import *

def base64encode(f):
    """
    Read the contents of a binary file and return a base64 encoded string.
    """
    s = ''
    data = f.read(3)

    while data:
        s += encode3bytes(data)
        data = f.read(3)

    return s


def base64decode(f):
    """
    """

    """
      >>> f1 = io.BytesIO(b'This is a test. It is only a test!')
      >>> from base64_tools import base64encode, base64decode
      >>> s = base64encode(f1)
      >>> s
      'VGhpcyBpcyBhIHRlc3QuIEl0IGlzIG9ubHkgYSB0ZXN0IQ=='
      >>> f2 = io.StringIO(s)
      >>> bs = base64decode(f2)
      >>> bs
      b'This is a test. It is only a test!'
    """


if __name__ == '__main__':
    import doctest
    doctest.testmod()

