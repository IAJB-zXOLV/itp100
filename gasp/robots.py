from gasp import *
begin_graphics()
finished = False


def place_robot():
    global robot_x, robot_y, robot_shape
    robot_x = random_between(0, 63)
    robot_y = random_between(0, 47)
    robot_shape = Box ((10 * robot_x, 10 * robot_y), 8, 8, color = color.BROWN)

def place_player():
    global player_x, player_y, player_shape, place_player
    player_x = random_between(0, 63)
    player_y = random_between(0,47)
    player_shape = Circle((player_x, player_y), 5, filled = True, color = color.BLUE)

def collided():
    global player_x, player_y, robot_x, robot_y
    if player_x == robot_x and player_y == robot_y:
        return True
    else:
        return False

def check_collided():
    if collided() == True:
        finished = True
        words = "You can't run forever dear human."
        message = Text(words, (320,240), color=color.BLACK, size=15)
        sleep(3)

def move_player():
    global player_x, player_y, player_shape, move_player

    sleep(0.05)

    key = update_when('key_pressed')

    if key == 'c':
        player_x = random_between(0, 63)
        player_y = random_between(0, 47)

    elif key == 'w' and player_y < 47:
        player_y += 1

    elif key == 'a' and player_x > 0:
        player_x -= 1

    elif key == 's' and player_y > 0:
        player_y -= 1

    elif key == 'd' and player_x < 63:
        player_x += 1

    elif key == 'q' and player_x > 0 and player_y < 47:
        player_x -= 1
        player_y += 1

    elif key == 'e' and player_x < 63 and player_y < 47:
        player_x += 1
        player_y += 1

    elif key == 'z' and player_x > 0 and player_y > 0:
        player_x -= 1
        player_y -= 1

    elif key == 'x' and player_x < 63 and player_y > 0:
        player_x += 1
        player_y -= 1

    move_to(player_shape, (10 * player_x + 5, 10 * player_y + 5))

def move_robot():
    global player_y, player_x, robot_x, robot_y, robot_shape
    if player_x < robot_x:
        if player_y > robot_y:
            robot_y += 1
            robot_x -= 1
        elif player_y < robot_y:
            robot_y -= 1
            robot_x -= 1
        elif player_y == robot_y:
            robot_x -= 1
    
    elif player_x > robot_x:
        if player_y > robot_y:
            robot_y += 1
            robot_x += 1
        elif player_y < robot_y:
            robot_y -= 1
            robot_x += 1
        elif player_y == robot_y:
            robot_x += 1

    elif player_x == robot_x:
        if player_y > robot_y:
            robot_y += 1
        elif player_y < robot_y:
            robot_y -= 1


    move_to(robot_shape, (10 * robot_x, 10 * robot_y))

place_player()
place_robot()

while not finished:
    move_player()
    move_robot()
    check_collided()

end_graphics()
